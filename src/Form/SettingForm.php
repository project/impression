<?php

/**
 * @file
 * Contains \Drupal\impression\Form\SettingForm.
 */

namespace Drupal\impression\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\CronInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form with examples on how to use cron.
 */
class SettingForm extends ConfigFormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cron service.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The state keyvalue collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, CronInterface $cron, QueueFactory $queue, StateInterface $state) {
    parent::__construct($config_factory);
    $this->currentUser = $current_user;
    $this->cron = $cron;
    $this->queue = $queue;
    $this->state = $state;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('cron'),
      $container->get('queue'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'impression';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('impression.settings');

    $form['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration of impression'),
      '#open' => TRUE,
    ];
    $form['configuration']['impression_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Time to keep the impression data.'),
      '#description' => $this->t('Time after which impression_cron will keep the impression data.'),
      '#default_value' => $config->get('interval'),
      '#options' => [
        0 => $this->t('Keep it forever'),
        3600 => $this->t('1 hour'),
        86400 => $this->t('1 day'),
        604800 => $this->t('1 week'),
        2592000 => $this->t('1 month'),
        31536000 => $this->t('1 year'),
      ],
    ];
    $form['configuration']['impression_start'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Start capture impression data.'),
      '#default_value' => $config->get('capture'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update the interval as stored in configuration. This will be read when
    // this modules hook_cron function fires and will be used to ensure that
    // action is taken only after the appropiate time has elapsed.
    $this->config('impression.settings')
      ->set('interval', $form_state->getValue('impression_interval'))
      ->set('capture', $form_state->getValue('impression_start'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['impression.settings'];
  }

}
